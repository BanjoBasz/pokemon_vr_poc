//Note: when using this code it is required that you have the same amount of boxes and texts
window.onload = () => {
  const box = document.getElementById('js--box');
  const pokemonTexts = document.getElementsByClassName('js--pokemon-text');
  const pokemonImages = document.getElementsByClassName('js--pokemon-picture');

  const getPokemon = (number,pokemonImage,pokemonText) => {
    let pokemon = fetch("https://pokeapi.co/api/v2/pokemon/" + number)
      .then( (data) => {
          return data.json();
      })
      .then( (response) => {
        changePokemon(response.sprites.front_default,response.name,pokemonImage,pokemonText);
      });
  }
    const changePokemon = (pokemonSprite,pokemonName,pokemonImage,pokemonText) => {
      pokemonImage.setAttribute("src",pokemonSprite);
      pokemonText.setAttribute("value",pokemonName.toUpperCase());
    }

    box.onmouseenter = (event) => {
      box.setAttribute('color','red');
      for(let i =0; i < pokemonImages.length; i++){
          let number = Math.floor(Math.random() * 150 + 1);
          getPokemon(number,pokemonImages[i],pokemonTexts[i]);
      }
    }

    box.onmouseleave = (event) => {
      box.setAttribute('color','green');
    }
}
